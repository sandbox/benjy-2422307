<?php

/**
 * @file
 * Contains
 */

namespace Drupal\iframe\Plugin\migrate\cckfield;

use Drupal\migrate\Entity\MigrationInterface;
use Drupal\migrate_drupal\Plugin\migrate\cckfield\CckFieldPluginBase;

/**
 * @PluginID("iframe")
 */
class IframeField extends CckFieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function processField(MigrationInterface $migration) {
    // The field would be iframe rather than link if it existed.
    $process[0]['map'][$this->pluginId][$this->pluginId] = 'link';
    $migration->mergeProcessOfProperty('type', $process);
  }

  /**
   * {@inheritdoc}
   */
  public function processFieldWidget(MigrationInterface $migration) {
    // The widget would be iframe rather than link if it existed.
    $process['type']['map'][$this->pluginId] = 'link_default';
    $migration->mergeProcessOfProperty('options/type', $process);
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldFormatterMap() {
    return [
      'default' => 'link',
      'iframeonly' => 'link',
      'asurl' => 'link',
      'asurl_withuri' => 'link',
      'hidden' => 'hidden',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function processCckFieldValues(MigrationInterface $migration, $field_name, $data) {
      // Add our custom processing for iFrames.
      $process = [
        'plugin' => 'iframe_field',
        'source' => [
          $field_name,
          $field_name . '_title',
          $field_name . '_attributes',
        ],
      ];
      $migration->mergeProcessOfProperty($field_name, $process);
  }

}
