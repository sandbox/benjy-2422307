<?php

/**
 * @file
 * Contains \Drupal\iframe\Plugin\migrate\process\d6\IframeField.
 */

namespace Drupal\iframe\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "iframe_field"
 * )
 */
class IframeField extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   *
   * Transform the field as required for an iFrame field.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    list($url, $title, $attributes) = $value;
    $iframe = [
      'uri' => $url,
      'title' => $title,
      'options' => ['attributes' => unserialize($attributes)],
    ];
    return $iframe;
  }

}
