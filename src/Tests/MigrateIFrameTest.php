<?php

/**
 * @file
 * Contains \Drupal\iframe\Tests\MigrateIFrameTest
 */

namespace Drupal\iframe\Tests;

use Drupal\migrate_drupal\Tests\MigrateDrupalTestBase;
use Drupal\migrate\MigrateExecutable;
use Drupal\node\Entity\Node;

/**
 * Test that we can migrate an iFrame field.
 *
 * @group migrate
 */
class MigrateIFrameTest extends MigrateDrupalTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('node', 'link', 'iframe');

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $dumps = [
      $this->getDumpDirectory() . '/ContentNodeField.php',
      $this->getDumpDirectory() . '/ContentNodeFieldInstance.php',
      $this->getDumpDirectory() . '/Node.php',
      $this->getDumpDirectory() . '/Filters.php',
      $this->getDumpDirectory() . '/FilterFormats.php',
      $this->getDumpDirectory() . '/NodeRevisions.php',
      $this->getDumpDirectory() . '/ContentTypePage.php',
      $this->getDumpDirectory() . '/NodeType.php',
      $this->getDumpDirectory() . '/Variable.php',
      $this->getDumpDirectory() . '/Users.php',
      $this->getDumpDirectory() . '/Permission.php',
      $this->getDumpDirectory() . '/UsersRoles.php',
      $this->getDumpDirectory() . '/Role.php',
    ];
    $this->loadDumps($dumps);

    $migration_ids = [
      'd6_field',
      'd6_field_instance',

      'd6_node_type',
      'd6_node_settings',
      'd6_filter_format',
      'd6_node',

      'd6_cck_field_values:*',
      'd6_field_formatter_settings',
      'd6_view_modes',
      'd6_field_instance_widget_settings',

      'd6_user',
      'd6_user_role',

    ];
    $migrations = entity_load_multiple('migration', $migration_ids);
    foreach ($migrations as $migration) {
      $executable = new MigrateExecutable($migration, $this);
      $executable->import();
    }
  }

  /**
   * Test iframe fields can be migrated.
   */
  public function testIframeField() {
    $node = Node::load(1);
    $this->assertIdentical($node->field_youtube->uri, 'http://drupal.org/iframe');
    $this->assertIdentical($node->field_youtube->title, 'Drupal org');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDumpDirectory() {
    return __DIR__ . '/Table';
  }

}
