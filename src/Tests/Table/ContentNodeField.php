<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\Dump\ContentNodeField.
 *
 * THIS IS A GENERATED FILE. DO NOT EDIT.
 *
 * @see cores/scripts/dump-database-d6.sh
 * @see https://www.drupal.org/sandbox/benjy/2405029
 */

namespace Drupal\iframe\Tests\Table;

use Drupal\migrate_drupal\Tests\Dump\DrupalDumpBase;

/**
 * Generated file to represent the content_node_field table.
 */
class ContentNodeField extends DrupalDumpBase {

  public function load() {
    $this->createTable("content_node_field", array(
      'primary key' => array(
        'field_name',
      ),
      'fields' => array(
        'field_name' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '32',
          'default' => '',
        ),
        'type' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '127',
          'default' => '',
        ),
        'global_settings' => array(
          'type' => 'text',
          'not null' => TRUE,
          'length' => 100,
        ),
        'required' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '4',
          'default' => '0',
        ),
        'multiple' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '4',
          'default' => '0',
        ),
        'db_storage' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '4',
          'default' => '1',
        ),
        'module' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '127',
          'default' => '',
        ),
        'db_columns' => array(
          'type' => 'text',
          'not null' => TRUE,
          'length' => 100,
        ),
        'active' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '4',
          'default' => '0',
        ),
        'locked' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '4',
          'default' => '0',
        ),
      ),
    ));
    $this->database->insert("content_node_field")->fields(array(
      'field_name',
      'type',
      'global_settings',
      'required',
      'multiple',
      'db_storage',
      'module',
      'db_columns',
      'active',
      'locked',
    ))
    ->values(array(
      'field_name' => 'field_youtube',
      'type' => 'iframe',
      'global_settings' => 'a:6:{s:10:"attributes";a:6:{s:5:"class";s:0:"";s:5:"width";s:4:"100%";s:6:"height";s:3:"700";s:11:"frameborder";s:1:"0";s:9:"scrolling";s:4:"auto";s:12:"transparency";s:1:"0";}s:7:"display";s:0:"";s:3:"url";i:0;s:5:"title";s:0:"";s:11:"title_value";s:0:"";s:13:"enable_tokens";s:0:"";}',
      'required' => '0',
      'multiple' => '0',
      'db_storage' => '1',
      'module' => 'iframe',
      'db_columns' => 'a:3:{s:3:"url";a:4:{s:4:"type";s:7:"varchar";s:6:"length";i:1024;s:8:"not null";b:0;s:8:"sortable";b:1;}s:5:"title";a:4:{s:4:"type";s:7:"varchar";s:6:"length";i:255;s:8:"not null";b:0;s:8:"sortable";b:1;}s:10:"attributes";a:3:{s:4:"type";s:4:"text";s:4:"size";s:6:"medium";s:8:"not null";b:0;}}',
      'active' => '1',
      'locked' => '0',
    ))->execute();
  }

}
