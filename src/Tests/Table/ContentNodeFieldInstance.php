<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\Dump\ContentNodeFieldInstance.
 *
 * THIS IS A GENERATED FILE. DO NOT EDIT.
 *
 * @see cores/scripts/dump-database-d6.sh
 * @see https://www.drupal.org/sandbox/benjy/2405029
 */

namespace Drupal\iframe\Tests\Table;

use Drupal\migrate_drupal\Tests\Dump\DrupalDumpBase;

/**
 * Generated file to represent the content_node_field_instance table.
 */
class ContentNodeFieldInstance extends DrupalDumpBase {

  public function load() {
    $this->createTable("content_node_field_instance", array(
      'primary key' => array(
        'field_name',
        'type_name',
      ),
      'fields' => array(
        'field_name' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '32',
          'default' => '',
        ),
        'type_name' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '32',
          'default' => '',
        ),
        'weight' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '11',
          'default' => '0',
        ),
        'label' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '255',
          'default' => '',
        ),
        'widget_type' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '32',
          'default' => '',
        ),
        'widget_settings' => array(
          'type' => 'text',
          'not null' => TRUE,
          'length' => 100,
        ),
        'display_settings' => array(
          'type' => 'text',
          'not null' => TRUE,
          'length' => 100,
        ),
        'description' => array(
          'type' => 'text',
          'not null' => TRUE,
          'length' => 100,
        ),
        'widget_module' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '127',
          'default' => '',
        ),
        'widget_active' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '4',
          'default' => '0',
        ),
      ),
    ));
    $this->database->insert("content_node_field_instance")->fields(array(
      'field_name',
      'type_name',
      'weight',
      'label',
      'widget_type',
      'widget_settings',
      'display_settings',
      'description',
      'widget_module',
      'widget_active',
    ))
    ->values(array(
      'field_name' => 'field_youtube',
      'type_name' => 'page',
      'weight' => '31',
      'label' => 'YouTube',
      'widget_type' => 'iframe',
      'widget_settings' => 'a:2:{s:13:"default_value";a:1:{i:0;a:3:{s:5:"title";s:0:"";s:3:"url";s:0:"";s:10:"attributes";a:5:{s:5:"width";s:0:"";s:6:"height";s:0:"";s:11:"frameborder";s:1:"0";s:9:"scrolling";s:4:"auto";s:12:"transparency";s:1:"0";}}}s:17:"default_value_php";N;}',
      'display_settings' => 'a:4:{s:5:"label";a:2:{s:6:"format";s:5:"above";s:7:"exclude";i:0;}s:6:"teaser";a:2:{s:6:"format";s:7:"default";s:7:"exclude";i:0;}s:4:"full";a:2:{s:6:"format";s:7:"default";s:7:"exclude";i:0;}i:4;a:2:{s:6:"format";s:7:"default";s:7:"exclude";i:0;}}',
      'description' => '',
      'widget_module' => 'iframe',
      'widget_active' => '1',
    ))->execute();
  }

}
