<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\Dump\ContentTypePage.
 *
 * THIS IS A GENERATED FILE. DO NOT EDIT.
 *
 * @see cores/scripts/dump-database-d6.sh
 * @see https://www.drupal.org/sandbox/benjy/2405029
 */

namespace Drupal\iframe\Tests\Table;

use Drupal\migrate_drupal\Tests\Dump\DrupalDumpBase;

/**
 * Generated file to represent the content_type_page table.
 */
class ContentTypePage extends DrupalDumpBase {

  public function load() {
    $this->createTable("content_type_page", array(
      'primary key' => array(
        'vid',
      ),
      'fields' => array(
        'vid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '10',
          'default' => '0',
          'unsigned' => TRUE,
        ),
        'nid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '10',
          'default' => '0',
          'unsigned' => TRUE,
        ),
        'field_youtube_url' => array(
          'type' => 'varchar',
          'not null' => FALSE,
          'length' => '1024',
        ),
        'field_youtube_title' => array(
          'type' => 'varchar',
          'not null' => FALSE,
          'length' => '255',
        ),
        'field_youtube_attributes' => array(
          'type' => 'text',
          'not null' => FALSE,
          'length' => 100,
        ),
      ),
    ));
    $this->database->insert("content_type_page")->fields(array(
      'vid',
      'nid',
      'field_youtube_url',
      'field_youtube_title',
      'field_youtube_attributes',
    ))
    ->values(array(
      'vid' => '1',
      'nid' => '1',
      'field_youtube_url' => 'http://drupal.org/iframe',
      'field_youtube_title' => 'Drupal org',
      'field_youtube_attributes' => 'a:5:{s:5:"width";s:4:"100%";s:6:"height";s:3:"700";s:11:"frameborder";s:1:"0";s:9:"scrolling";s:4:"auto";s:12:"transparency";s:1:"0";}',
    ))->execute();
  }

}
