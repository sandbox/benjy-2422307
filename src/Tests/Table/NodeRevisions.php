<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\Dump\NodeRevisions.
 *
 * THIS IS A GENERATED FILE. DO NOT EDIT.
 *
 * @see cores/scripts/dump-database-d6.sh
 * @see https://www.drupal.org/sandbox/benjy/2405029
 */

namespace Drupal\iframe\Tests\Table;

use Drupal\migrate_drupal\Tests\Dump\DrupalDumpBase;

/**
 * Generated file to represent the node_revisions table.
 */
class NodeRevisions extends DrupalDumpBase {

  public function load() {
    $this->createTable("node_revisions", array(
      'fields' => array(
        'nid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '10',
          'default' => '0',
          'unsigned' => TRUE,
        ),
        'vid' => array(
          'type' => 'serial',
          'not null' => TRUE,
          'length' => '10',
          'unsigned' => TRUE,
        ),
        'uid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '11',
          'default' => '0',
        ),
        'title' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '255',
          'default' => '',
        ),
        'body' => array(
          'type' => 'text',
          'not null' => TRUE,
          'length' => 100,
        ),
        'teaser' => array(
          'type' => 'text',
          'not null' => TRUE,
          'length' => 100,
        ),
        'log' => array(
          'type' => 'text',
          'not null' => TRUE,
          'length' => 100,
        ),
        'timestamp' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '11',
          'default' => '0',
        ),
        'format' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '11',
          'default' => '0',
        ),
      ),
      'primary key' => array(
        'vid',
      ),
    ));
    $this->database->insert("node_revisions")->fields(array(
      'nid',
      'vid',
      'uid',
      'title',
      'body',
      'teaser',
      'log',
      'timestamp',
      'format',
    ))
    ->values(array(
      'nid' => '1',
      'vid' => '1',
      'uid' => '1',
      'title' => 'Test iFrame Page',
      'body' => '',
      'teaser' => '',
      'log' => '',
      'timestamp' => '1423386436',
      'format' => '1',
    ))->execute();
  }

}
