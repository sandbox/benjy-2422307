<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\Dump\Permission.
 *
 * THIS IS A GENERATED FILE. DO NOT EDIT.
 *
 * @see cores/scripts/dump-database-d6.sh
 * @see https://www.drupal.org/sandbox/benjy/2405029
 */

namespace Drupal\iframe\Tests\Table;

use Drupal\migrate_drupal\Tests\Dump\DrupalDumpBase;

/**
 * Generated file to represent the permission table.
 */
class Permission extends DrupalDumpBase {

  public function load() {
    $this->createTable("permission", array(
      'primary key' => array(
        'pid',
      ),
      'fields' => array(
        'pid' => array(
          'type' => 'serial',
          'not null' => TRUE,
          'length' => '11',
        ),
        'rid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '10',
          'default' => '0',
          'unsigned' => TRUE,
        ),
        'perm' => array(
          'type' => 'text',
          'not null' => FALSE,
          'length' => 100,
        ),
        'tid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '10',
          'default' => '0',
          'unsigned' => TRUE,
        ),
      ),
    ));
    $this->database->insert("permission")->fields(array(
      'pid',
      'rid',
      'perm',
      'tid',
    ))
    ->values(array(
      'pid' => '1',
      'rid' => '1',
      'perm' => 'access content',
      'tid' => '0',
    ))->values(array(
      'pid' => '2',
      'rid' => '2',
      'perm' => 'access comments, access content, post comments, post comments without approval',
      'tid' => '0',
    ))->execute();
  }

}
