<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\Dump\Users.
 *
 * THIS IS A GENERATED FILE. DO NOT EDIT.
 *
 * @see cores/scripts/dump-database-d6.sh
 * @see https://www.drupal.org/sandbox/benjy/2405029
 */

namespace Drupal\iframe\Tests\Table;

use Drupal\migrate_drupal\Tests\Dump\DrupalDumpBase;

/**
 * Generated file to represent the users table.
 */
class Users extends DrupalDumpBase {

  public function load() {
    $this->createTable("users", array(
      'primary key' => array(
        'uid',
      ),
      'fields' => array(
        'uid' => array(
          'type' => 'serial',
          'not null' => TRUE,
          'length' => '10',
          'unsigned' => TRUE,
        ),
        'name' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '60',
          'default' => '',
        ),
        'pass' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '32',
          'default' => '',
        ),
        'mail' => array(
          'type' => 'varchar',
          'not null' => FALSE,
          'length' => '64',
          'default' => '',
        ),
        'mode' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '4',
          'default' => '0',
        ),
        'sort' => array(
          'type' => 'int',
          'not null' => FALSE,
          'length' => '4',
          'default' => '0',
        ),
        'threshold' => array(
          'type' => 'int',
          'not null' => FALSE,
          'length' => '4',
          'default' => '0',
        ),
        'theme' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '255',
          'default' => '',
        ),
        'signature' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '255',
          'default' => '',
        ),
        'signature_format' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '6',
          'default' => '0',
        ),
        'created' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '11',
          'default' => '0',
        ),
        'access' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '11',
          'default' => '0',
        ),
        'login' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '11',
          'default' => '0',
        ),
        'status' => array(
          'type' => 'int',
          'not null' => TRUE,
          'length' => '4',
          'default' => '0',
        ),
        'timezone' => array(
          'type' => 'varchar',
          'not null' => FALSE,
          'length' => '8',
        ),
        'language' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '12',
          'default' => '',
        ),
        'picture' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '255',
          'default' => '',
        ),
        'init' => array(
          'type' => 'varchar',
          'not null' => FALSE,
          'length' => '64',
          'default' => '',
        ),
        'data' => array(
          'type' => 'text',
          'not null' => FALSE,
          'length' => 100,
        ),
      ),
    ));
    $this->database->insert("users")->fields(array(
      'uid',
      'name',
      'pass',
      'mail',
      'mode',
      'sort',
      'threshold',
      'theme',
      'signature',
      'signature_format',
      'created',
      'access',
      'login',
      'status',
      'timezone',
      'language',
      'picture',
      'init',
      'data',
    ))
    ->execute();
  }

}
