<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\Dump\Variable.
 *
 * THIS IS A GENERATED FILE. DO NOT EDIT.
 *
 * @see cores/scripts/dump-database-d6.sh
 * @see https://www.drupal.org/sandbox/benjy/2405029
 */

namespace Drupal\iframe\Tests\Table;

use Drupal\migrate_drupal\Tests\Dump\DrupalDumpBase;

/**
 * Generated file to represent the variable table.
 */
class Variable extends DrupalDumpBase {

  public function load() {
    $this->createTable("variable", array(
      'primary key' => array(
        'name',
      ),
      'fields' => array(
        'name' => array(
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => '128',
          'default' => '',
        ),
        'value' => array(
          'type' => 'text',
          'not null' => TRUE,
          'length' => 100,
        ),
      ),
    ));
    $this->database->insert("variable")->fields(array(
      'name',
      'value',
    ))
    ->values(array(
      'name' => 'clean_url',
      'value' => 's:1:"1";',
    ))->values(array(
      'name' => 'comment_page',
      'value' => 'i:0;',
    ))->values(array(
      'name' => 'content_extra_weights_page',
      'value' => 'a:7:{s:5:"title";s:2:"-5";s:10:"body_field";s:1:"0";s:20:"revision_information";s:2:"20";s:6:"author";s:2:"20";s:7:"options";s:2:"25";s:16:"comment_settings";s:2:"30";s:4:"menu";s:2:"-2";}',
    ))->values(array(
      'name' => 'content_schema_version',
      'value' => 'i:6009;',
    ))->values(array(
      'name' => 'css_js_query_string',
      'value' => 's:20:"O0000000000000000000";',
    ))->values(array(
      'name' => 'date_default_timezone',
      'value' => 's:5:"28800";',
    ))->values(array(
      'name' => 'drupal_http_request_fails',
      'value' => 'b:0;',
    ))->values(array(
      'name' => 'drupal_private_key',
      'value' => 's:43:"GNASCVFI3eqn7x59gwra73keubriln4uHhHSAKIxkrw";',
    ))->values(array(
      'name' => 'file_directory_temp',
      'value' => 's:4:"/tmp";',
    ))->values(array(
      'name' => 'filter_html_1',
      'value' => 'i:1;',
    ))->values(array(
      'name' => 'install_profile',
      'value' => 's:7:"default";',
    ))->values(array(
      'name' => 'install_task',
      'value' => 's:4:"done";',
    ))->values(array(
      'name' => 'install_time',
      'value' => 'i:1423386350;',
    ))->values(array(
      'name' => 'javascript_parsed',
      'value' => 'a:0:{}',
    ))->values(array(
      'name' => 'menu_expanded',
      'value' => 'a:0:{}',
    ))->values(array(
      'name' => 'menu_masks',
      'value' => 'a:19:{i:0;i:127;i:1;i:63;i:2;i:62;i:3;i:61;i:4;i:59;i:5;i:31;i:6;i:30;i:7;i:29;i:8;i:24;i:9;i:21;i:10;i:15;i:11;i:14;i:12;i:11;i:13;i:7;i:14;i:6;i:15;i:5;i:16;i:3;i:17;i:2;i:18;i:1;}',
    ))->values(array(
      'name' => 'node_options_forum',
      'value' => 'a:1:{i:0;s:6:"status";}',
    ))->values(array(
      'name' => 'node_options_page',
      'value' => 'a:1:{i:0;s:6:"status";}',
    ))->values(array(
      'name' => 'site_mail',
      'value' => 's:20:"ben@dougherty.net.au";',
    ))->values(array(
      'name' => 'site_name',
      'value' => 's:9:"localhost";',
    ))->values(array(
      'name' => 'theme_default',
      'value' => 's:7:"garland";',
    ))->values(array(
      'name' => 'theme_settings',
      'value' => 'a:1:{s:21:"toggle_node_info_page";b:0;}',
    ))->values(array(
      'name' => 'update_last_check',
      'value' => 'i:1423386377;',
    ))->values(array(
      'name' => 'user_email_verification',
      'value' => 'b:1;',
    ))->execute();
  }

}
